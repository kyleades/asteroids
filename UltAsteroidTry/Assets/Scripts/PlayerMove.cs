﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

    // transform var
    private Transform tf;
    // movement speed var
    public float moveSpeed;
    // rotation speed var
    public float turnSpeed;

	// Use this for initialization
	void Start ()
    {
        // grab transform into a var
        tf = GetComponent<Transform>();

	}
	
	// Update is called once per frame
	void Update ()
    {
		if (Input.GetKey(KeyCode.RightArrow))
        {
            // spin the ship right
            tf.Rotate(0, 0, -turnSpeed * Time.deltaTime);

        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            // spin the ship left
            tf.Rotate(0, 0, turnSpeed * Time.deltaTime);

        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            // move the ship forward
            tf.position += (tf.right * moveSpeed * Time.deltaTime);

        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            // move the ship forward
            tf.position -= (tf.right * moveSpeed * Time.deltaTime);

        }
        // fire weapon
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject bullet = Instantiate<GameObject>(GameManager.instance.bullet);
            bullet.transform.position = transform.position;
        }
    }
    // 
    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.tag == "badguy")
        {
        
            LifeLost();
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Map")
        {
            LifeLost();
        }
    }

    void LifeLost()
    {
        GameManager.instance.numLives--;
        if (GameManager.instance.numLives == 0)
        {
            // Game over
            Application.Quit();
        }
        else
        {
            GameManager.instance.RemoveEnemies();
            tf.transform.position = new Vector3(0, 0, 0);
        }
    }
}
