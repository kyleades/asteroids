﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    // Make the game manager
    public static GameManager instance;

    // var for holding spawn points
    public GameObject[] spawnPoints;

    // Badguy movement speed
    public float badguySpeed;

    // var for holding enemies
    public List<GameObject> enemies;

    // Bullet prefab and stats
    public GameObject bullet;
    public float bulletSpeed;
    public float bulletLife;

    // Enemy hunter
    public float enemySpeed;
    public float enemyRotationSpeed;


    // List of enemies
    public List<GameObject> activeEnemies;
    public bool removingEnemies;
    public int maximumNumberActiveEnemies;

    public GameObject player;

    public int numLives;


    // Create the Singleton instance
    void Awake()
    {
        // As long as there is not an instance already set
        if (instance == null)
        {
            // store this instance in a var
            instance = this;
            // Don't delete this object if we load a new scene
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start()
    {
        activeEnemies = new List<GameObject>();
        removingEnemies = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (activeEnemies.Count < maximumNumberActiveEnemies)
        {
            // Determine spawn point
            int id = Random.Range(0, spawnPoints.Length);
            GameObject point = spawnPoints[id];

            // Determine which enemy to spawn
            GameObject enemy = enemies[Random.Range(0, enemies.Count)];

            // Instantiate an enemy
            GameObject enemyInstance = Instantiate<GameObject>(enemy, point.transform.position, Quaternion.identity);

            // Add to enemies list
            activeEnemies.Add(enemyInstance);
        }

    }


    public void RemoveEnemies()
    {
        removingEnemies = true;
        for (int i = 0; i < activeEnemies.Count; i++)
        {
            Destroy(activeEnemies[i]);
        }
        activeEnemies.Clear();
        removingEnemies = false;
    }
}
