﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FacesPlayer : MonoBehaviour {

    Vector3 direction;

    Transform player;

    // Use this for initialization
    void Start()
    {
        //direction = new Vector3(1, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (player == null)
        {

            GameObject go = GameObject.Find("Player");

            if (go !=null)
            {
                player = go.transform;
            }
        }

        direction = new Vector3(0, 1, 0);
        transform.Translate(direction * Time.deltaTime * GameManager.instance.enemySpeed);

        direction = player.position - transform.position;
        direction.Normalize();

        float zAngle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90;

        Quaternion desiredRot = Quaternion.Euler(0, 0, zAngle);

        transform.rotation = Quaternion.RotateTowards(transform.rotation, desiredRot, GameManager.instance.enemyRotationSpeed * Time.deltaTime);

    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            GameManager.instance.activeEnemies.Remove(this.gameObject);
            Destroy(this.gameObject);

            // Also destroy bullet
            Destroy(other.gameObject);
        }
    }
}