﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour {

    public Vector3 dir;
    Transform player;

    /*
     * 
     */

    void Start()
    {
        if (player == null)
        {

            GameObject go = GameObject.Find("Player");

            if (go != null)
            {
                player = go.transform;
            }
        }

        dir = player.position - transform.position;
        dir.Normalize();
        //float zAngle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg -180;
        //transform.rotation = Quaternion.Euler(0, 0, zAngle);
        

    }

    // Update is called once per frame
    void Update () {
        // move the enemy forward
        
        transform.Translate(dir * Time.deltaTime * GameManager.instance.badguySpeed);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            GameManager.instance.activeEnemies.Remove(this.gameObject);
            Destroy(this.gameObject);

            // Also destroy bullet
            Destroy(other.gameObject);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (!GameManager.instance.removingEnemies)
        {
            if (other.gameObject.tag == "Map")
            {
                GameManager.instance.activeEnemies.Remove(this.gameObject);
                Destroy(this.gameObject);
            }
        }
    }
}
